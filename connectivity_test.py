from pyats import aetest

class CommonSetup(aetest.CommonSetup):
    """ Common Setup Section """
    @aetest.subsection
    def check_topology(self, testbed,
                       ios_device1='SOHO',
                       ios_device2='ISP1',
                       ios_device3='ISP2'):
        """ Checking topology mapping """
        router_soho = testbed.devices[ios_device1]
        router_isp1 = testbed.devices[ios_device2]
        router_isp2 = testbed.devices[ios_device3]

        self.parent.parameters.update(router_soho=router_soho,
                                      router_isp1=router_isp1,
                                      router_isp2=router_isp2)

        links = [router_soho.find_links(router_isp1),
                 router_isp1.find_links(router_isp2),
                 router_isp2.find_links(router_soho)]
        assert len(links) >= 3, 'at least one link is required between each device'


    @aetest.subsection
    def establish_connections(self, steps, router_soho, router_isp1, router_isp2):
        """ Checking possibility for connection to all devices  """
        with steps.start('Connecting to %s' % router_soho.name):
            router_soho.connect()

        with steps.start('Connecting to %s' % router_isp1.name):
            router_isp1.connect()

        with steps.start('Connecting to %s' % router_isp2.name):
            router_isp2.connect()


class PingTestCase(aetest.Testcase):
    """" Test possibility to pinging between devices """
    @aetest.setup
    def setup(self):
        """" Setup for PingTestCase """
        pass

    @aetest.test
    def ping(self):
        """" Method to ping devices """
        pass


class CommonCleanup(aetest.CommonCleanup):
    """ Disconnect from all devices """
    @aetest.subsection
    def disconnect(self, steps, router_soho, router_isp1, router_isp2):
        with steps.start('Disconnecting from %s' % router_soho.name):
            router_soho.disconnect()

        with steps.start('Disconnecting from %s' % router_isp1.name):
            router_isp1.disconnect()

        with steps.start('Disconnecting from %s' % router_isp2.name):
            router_isp2.disconnect()


if __name__ == '__main__':
    import argparse
    from pyats.topology import loader
    parser = argparse.ArgumentParser()
    parser.add_argument('--testbed', dest='testbed',
                        type=loader.load)
    args, unknown = parser.parse_known_args()
    aetest.main(**vars(args))
